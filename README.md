# Lufi cli

This a cli client for [Lufi](https://framagit.org/luc/lufi) written in Javascript.
You will need NodeJS for it to work.

## How to install

It will be available as a NPM package, so you will just have to do:

```
npm install -g lufi
```

## How to use

Just do `lufi -h` and you will know.

## How to use from this repository

* Ensure you are running a recent node version (current stable v7.2.1). You can easyly upgrade node using `n` package and its command `n stable`
* Install dependencies using `npm install`
* Run `./bin/lufi.js -h`

## How to contribute

Have a look at [CONTRIBUTING.md](CONTRIBUTING.md).

## License

GPLv3. See [LICENSE](LICENSE) for details
